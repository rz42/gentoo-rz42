# Copyright 2020-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit kernel-build

MY_P=linux-${PV%.*}
GENPATCHES_P=genpatches-${PV%.*}-$(( ${PV##*.} + 8 ))

DESCRIPTION="Linux kernel built with Gentoo patches"
HOMEPAGE="https://www.kernel.org/"
SRC_URI+=" https://cdn.kernel.org/pub/linux/kernel/v$(ver_cut 1).x/${MY_P}.tar.xz
	https://dev.gentoo.org/~mpagano/dist/genpatches/${GENPATCHES_P}.base.tar.xz
	https://dev.gentoo.org/~mpagano/dist/genpatches/${GENPATCHES_P}.extras.tar.xz"
S=${WORKDIR}/${MY_P}

LICENSE="GPL-2"
KEYWORDS="~amd64"

DEPEND="
	sys-kernel/dracut-crypt-ssh"

IUSE="debug"

RDEPEND="
	!sys-kernel/gentoo-kernel:${SLOT}
	!sys-kernel/gentoo-kernel-bin:${SLOT}
	!sys-kernel/vanilla-kernel:${SLOT}
	!sys-kernel/vanilla-kernel-bin:${SLOT}"
PDEPEND="
	>=virtual/dist-kernel-${PV}"

pkg_pretend() {
	if use initramfs; then
		elog
		elog "You can rebuild the initramfs via issuing a command equivalent to:"
		elog
		elog "	emerge --config ${CATEGORY}/${PN}"
		elog
	fi

	kernel-install_pkg_pretend
}

src_prepare() {
	local PATCHES=(
		# meh, genpatches have no directory
		"${WORKDIR}"/*.patch
	)
	default

	# prepare the default config
	case ${ARCH} in
		amd64)
			cp "${FILESDIR}"/config-"${PV}" .config || die
			;;
		*)
			die "Unsupported arch ${ARCH}"
			;;
	esac
}

pkg_postinst() {
	if [ -f /boot/initramfs-"${PV}".img ]; then
		mv /boot/initramfs-"${PV}".img /boot/initramfs-"${PV}".img.old
	fi
	if [ $(readlink /boot/initramfs) = initramfs-"${PV}".img ]; then
		ln -sf initramfs-"${PV}".img.old /boot/initramfs.old
	else
		mv /boot/initramfs /boot/initramfs.old
	fi
	depmod "${PV}${KV_LOCALVERSION}"
	kernel-install_pkg_postinst
	ln -sf initramfs-"${PV}".img /boot/initramfs
}

pkg_config() {
	if [ -f /boot/initramfs-"${PV}".img ]; then
		mv /boot/initramfs-"${PV}".img /boot/initramfs-"${PV}".img.old
	fi
	if [ $(readlink /boot/initramfs) = initramfs-"${PV}".img ]; then
		ln -sf initramfs-"${PV}".img.old /boot/initramfs.old
	else
		mv /boot/initramfs /boot/initramfs.old
	fi
	depmod "${PV}${KV_LOCALVERSION}"
	kernel-install_pkg_config
	ln -sf initramfs-"${PV}".img /boot/initramfs
}
